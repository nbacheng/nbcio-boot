Nbcio-Boot V1.0.0 NBCIO亿事达企业管理平台
===============
[![码云Gitee](https://gitee.com/nbacheng/nbcio-boot/badge/star.svg?theme=blue)](https://gitee.com/nbacheng/nbcio-boot)
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitee.com/nbacheng/nbcio-boot/blob/master/LICENSE)
[![](https://img.shields.io/badge/Author-宁波阿成-orange.svg)](http://218.75.87.38:9888/)
[![](https://img.shields.io/badge/Blog-个人博客-blue.svg)](https://nbacheng.blog.csdn.net)
[![](https://img.shields.io/badge/version-1.0.0-brightgreen.svg)](https://gitee/nbacheng/nbcio-boot)
[![使用STS开发维护](https://img.shields.io/badge/STS-提供支持-blue.svg)](https://spring.io/tools)
[![Spring Boot](https://img.shields.io/badge/Spring%20Boot-2.7-blue.svg)]()
[![JDK-8+](https://img.shields.io/badge/JDK-8-green.svg)]()

- 基于jeecg-boot3.0版本（发布日期：2021-11-01）；

- 因License变化,需要咨询最新vue3版本技术支持服务的请联系作者,视频如下：
- B站地址：https://space.bilibili.com/3546758382618869

- 更多技术支持与服务请加入我的知识星球，名称:亿事达nbcio技术交流社区https://t.zsxq.com/iPi8F

前端地址 https://gitee.com/nbacheng/nbcio-vue

## 在线演示(包括H5) ：[http://218.75.87.38:9888](http://218.75.87.38:9888)

    H5地址是：http://218.75.87.38:9888/h5/
    演示账号：目前演示用户只能通过gitee授权免密码登录，或进群咨询
	如果你对项目感兴趣，请Watch、Star项目

## 联系作者
- 声明一下：现在、未来都不会有商业版本

- 有商业应用的公司请知会作者(免费使用，以后只做推广开源项目用)

- 欢迎bug反馈，需求建议，技术交流等

- 个人网页:  https://nbacheng.blog.csdn.net/

- QQ交流群1(已满): 655054809  QQ交流群2: 892227522
- 微信：![微信](https://oscimg.oschina.net/oscnet/up-b3a9af64dbe74a868a021ea16e1a55317d0.jpg)

## 友情链接
- Ruoyi-NBCIO项目: https://gitee.com/nbacheng/ruoyi-nbcio
- Uniapp项目: https://gitee.com/pzy332/jeecg-app-flowable.git 

## 后端技术架构
- 基础框架：Spring Boot 2.7.18

- 持久层框架：Mybatis-plus 3.5.5, JSqlParser4.6

- 报表工具： JimuReport 1.6.6

- 安全框架：Apache Shiro 1.13.0，Jwt 3.11.0

- 数据库连接池：阿里巴巴Druid 1.1.22

- 缓存框架：redis

- 日志打印：logback

- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。



## 开发环境

- 语言：Java 8

- IDE(JAVA)： STS安装lombok插件 或者 IDEA

- 依赖管理：Maven

- 数据库：MySQL5.7+  &  Oracle 11g & SqlServer & postgresql & 国产等更多数据库

- 缓存：Redis


注意： 如果本地安装了mysql和redis,启动容器前先停掉本地服务，不然会端口冲突。
       net stop redis
       net stop mysql
 
# 1.配置host

    # nbcioboot
    127.0.0.1   nbcio-boot-redis
    127.0.0.1   nbcio-boot-mysql
    127.0.0.1   nbcio-boot-system
	
# 2.修改项目配置文件 application.yml
    active: dev
	
# 3.修改application-dev.yml文件的数据库和redis链接
	修改数据库连接和redis连接，将连接改成host方式

# 4.先进JAVA项目nbcio-boot根路径 maven打包
    mvn clean package

# 5.访问后台项目（注意要开启swagger）
    http://localhost:8080/nbcio-boot/doc.html

## 开源的主要功能

   1、基于flowable 6.7.2 的工作流管理:
          包括流程设计、表单定义、流程发起、流程流转和消息提醒等功能，同时支持自定义业务的流程定义与流转。

   2、基于钉钉的薪资流程审批例子:
          写了一个薪资的钉钉流程流转，通过定义流程，同时结合钉钉，发起后通过钉钉来进行审批与流转。

   3、写了一个前端实现从表ERP格式选择，以便以后满足库存管理等ERP应用。

   4、参考了多个开源项目，在此表示感谢。
   
   5、增加了一个聊天功能
   
   6、在线表单设计器修改成formdesigner
   
   7、流程设计器修改成bpmn-process-designer
   
   8、支持online表单的流程申请与流转审批
   
   9、重新整理优化流程，支持会签角色等功能
   
   10、增加部分ERP功能
   
   11、增加大屏设计功能
   
   12、增加网盘功能
   
   13、增加项目管理功能

   14、以后希望能增加更多OA和ERP等相关功能。
   
   15、在所有任务菜单里支持流程的复活
   
   16、在所有任务菜单里支持动态新增用户任务（包括多实例）功能
   
   17、支持流程模型的版本管理
   
   18、支持流程模型的复制功能
   
   19、增加支持自定义业务表单多个相同服务的流程，就是发起流程用户可以选择其中的一个
   
   20、子流程的多实例功能
   
   21、大屏设计器支持数据集的显示
   
   22、以后陆续增加更多功能
   
## 捐赠 

如果觉得还不错，请作者喝杯咖啡吧！

![](https://oscimg.oschina.net/oscnet/up-58088c35672c874bd5a95c2327300d44dca.png)
   
   系统效果
   ----
![](https://oscimg.oschina.net/oscnet/up-ce7d9e52f39df3c7c9c08fae2233b843c86.png)

![](https://oscimg.oschina.net/oscnet/up-5a590c9f230541b58d89a3d44c23de7ae2d.png)

![](https://oscimg.oschina.net/oscnet/up-0b37ae7cf543b1ccc92f04a89c242866a25.png)

![](https://oscimg.oschina.net/oscnet/up-a5dcf863f39bb9bb81493b71eb46b51b884.png)

![](https://oscimg.oschina.net/oscnet/up-971d9321e22618ad70db56bbc1a6de77e9c.png)

![](https://oscimg.oschina.net/oscnet/up-e9182f6d379a37f8a03f347beeeca4cc7ca.png)

![](https://oscimg.oschina.net/oscnet/up-d575a8dd5bdf6747bcb3b6cb9b82c677810.png)

![](https://oscimg.oschina.net/oscnet/up-639599563b744e92ee9533f5e040d37c6ce.png)

![](https://oscimg.oschina.net/oscnet/up-1cca12b07f43edf134df5df66c9781972aa.png)

![](https://oscimg.oschina.net/oscnet/up-d2b8047ace2640dd190891fb78c3a58fd7e.png)

![](https://oscimg.oschina.net/oscnet/up-db16aa50ad36d0e6acf64034db270aefb5a.png)

![](https://oscimg.oschina.net/oscnet/up-088311c5367c583703473389478514c653f.png)

![](https://oscimg.oschina.net/oscnet/up-9e800cbcd809a118b90cd8925187ff9633d.png)

![](https://oscimg.oschina.net/oscnet/up-71a44b1e44c656155b90eadd64edf3ce2a0.png)

![](https://oscimg.oschina.net/oscnet/up-dfb183d37a7ac8b7c66af4e84fc120da1e1.png)

![](https://oscimg.oschina.net/oscnet/up-9f41d75e82ff682061dcfaec0ff6cb3954d.png)

![](https://oscimg.oschina.net/oscnet/up-6af11135ef1e923ef9f75af716886bc1b51.png)

## 推荐
大家在使用本项目时，推荐结合贺波老师的书
[《深入Flowable流程引擎：核心原理与高阶实战》](https://item.jd.com/14804836.html)学习。这本书得到了Flowable创始人Tijs Rademakers亲笔作序推荐，对系统学习和深入掌握Flowable的用法非常有帮助。
![](doc/flowable.jpg)

package com.nbcio.modules.flowable.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 *  动态新增任务节点
 *  nbacheng
 */
@Data
@ApiModel("动态新增任务节点--请求参数")
public class AddTaskVo {
	@ApiModelProperty("节点名称")
    private String name;

    @ApiModelProperty("数据Id")
    private String dataId;
    
    @ApiModelProperty("任务Id")
    private String taskId;

    @ApiModelProperty("流程实例Id")
    private String instanceId;

    @ApiModelProperty("审批人")
    private String assignee;
    
    @ApiModelProperty("是否是多实例")
    private boolean bmultiinstance; //布尔值必须小写，否则有问题
    
    @ApiModelProperty("是否串号还是并行")
    private boolean bsequential; //布尔值必须小写，否则有问题
    
    @ApiModelProperty("多实例完成条件")
    private String completionCondition;

    @ApiModelProperty("候选人")
    private List<String> candidateUsers;

    @ApiModelProperty("审批组")
    private List<String> candidateGroups;
    
    @ApiModelProperty("自定义业务主键")  
    private String businessKey;
    
    @ApiModelProperty("流程类型")
    private String category;
    
    @ApiModelProperty("应用类型")
    private String appType; 
    
}

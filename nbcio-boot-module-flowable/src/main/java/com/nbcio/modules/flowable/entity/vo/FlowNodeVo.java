package com.nbcio.modules.flowable.entity.vo;

import lombok.Data;

/**
 * @Description: 流程定义的用户任务节点信息
 * @Author: nbacheng
 * @Date:   2024-09-21
 * @Version: V1.0
 */
@Data
public class FlowNodeVo {
	/**
     * 用户任务节点key
     */
    private String nodeKey;

    /**
     * 用户任务节点名称
     */
    private String nodeName;
    
    /**
     * 办理人类型 assignee, candidateUsers, candidateGroups
     */
    private String type;
    /**
     * 办理人名称
     */
    private String assigneeName;
}

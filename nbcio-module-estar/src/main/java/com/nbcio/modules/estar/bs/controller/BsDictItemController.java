package com.nbcio.modules.estar.bs.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.nbcio.modules.estar.bs.entity.BsDictItem;
import com.nbcio.modules.estar.bs.service.IBsDictItemService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: bs_dict_item
 * @Author: nbacheng
 * @Date:   2024-08-21
 * @Version: V1.0
 */
@Api(tags="bs_dict_item")
@RestController
@RequestMapping("/bs/bsDictItem")
@Slf4j
public class BsDictItemController extends JeecgController<BsDictItem, IBsDictItemService> {
	@Autowired
	private IBsDictItemService bsDictItemService;
	
	/**
	 * 分页列表查询
	 *
	 * @param bsDictItem
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "bs_dict_item-分页列表查询")
	@ApiOperation(value="bs_dict_item-分页列表查询", notes="bs_dict_item-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(BsDictItem bsDictItem,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<BsDictItem> queryWrapper = QueryGenerator.initQueryWrapper(bsDictItem, req.getParameterMap());
		Page<BsDictItem> page = new Page<BsDictItem>(pageNo, pageSize);
		IPage<BsDictItem> pageList = bsDictItemService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param bsDictItem
	 * @return
	 */
	@AutoLog(value = "bs_dict_item-添加")
	@ApiOperation(value="bs_dict_item-添加", notes="bs_dict_item-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody BsDictItem bsDictItem) {
		bsDictItemService.save(bsDictItem);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param bsDictItem
	 * @return
	 */
	@AutoLog(value = "bs_dict_item-编辑")
	@ApiOperation(value="bs_dict_item-编辑", notes="bs_dict_item-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody BsDictItem bsDictItem) {
		bsDictItemService.updateById(bsDictItem);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "bs_dict_item-通过id删除")
	@ApiOperation(value="bs_dict_item-通过id删除", notes="bs_dict_item-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		bsDictItemService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "bs_dict_item-批量删除")
	@ApiOperation(value="bs_dict_item-批量删除", notes="bs_dict_item-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.bsDictItemService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "bs_dict_item-通过id查询")
	@ApiOperation(value="bs_dict_item-通过id查询", notes="bs_dict_item-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		BsDictItem bsDictItem = bsDictItemService.getById(id);
		if(bsDictItem==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(bsDictItem);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param bsDictItem
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, BsDictItem bsDictItem) {
        return super.exportXls(request, bsDictItem, BsDictItem.class, "bs_dict_item");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, BsDictItem.class);
    }

}

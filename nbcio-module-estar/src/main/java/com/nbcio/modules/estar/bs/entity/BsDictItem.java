package com.nbcio.modules.estar.bs.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: bs_dict_item
 * @Author: nbacheng
 * @Date:   2024-08-21
 * @Version: V1.0
 */
@Data
@TableName("bs_dict_item")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="bs_dict_item对象", description="bs_dict_item")
public class BsDictItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**数据字典编码*/
	@Excel(name = "数据字典编码", width = 15)
    @ApiModelProperty(value = "数据字典编码")
    private java.lang.String dictCode;
	/**字典项名称*/
	@Excel(name = "字典项名称", width = 15)
    @ApiModelProperty(value = "字典项名称")
    private java.lang.String itemName;
	/**字典项值*/
	@Excel(name = "字典项值", width = 15)
    @ApiModelProperty(value = "字典项值")
    private java.lang.String itemValue;
	/**字典扩展项*/
	@Excel(name = "字典扩展项", width = 15)
    @ApiModelProperty(value = "字典扩展项")
    private java.lang.String itemExtend;
	/**1:启用 0:禁用*/
	@Excel(name = "1:启用 0:禁用", width = 15)
    @ApiModelProperty(value = "1:启用 0:禁用")
    private java.lang.Integer enabled;
	/**语言标识*/
	@Excel(name = "语言标识", width = 15)
    @ApiModelProperty(value = "语言标识")
    private java.lang.String locale;
	/**描述*/
	@Excel(name = "描述", width = 15)
    @ApiModelProperty(value = "描述")
    private java.lang.String remark;
	/**排序*/
	@Excel(name = "排序", width = 15)
    @ApiModelProperty(value = "排序")
    private java.lang.Integer sort;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新用户*/
    @ApiModelProperty(value = "更新用户")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**版本*/
	@Excel(name = "版本", width = 15)
    @ApiModelProperty(value = "版本")
    private java.lang.Integer version;
}

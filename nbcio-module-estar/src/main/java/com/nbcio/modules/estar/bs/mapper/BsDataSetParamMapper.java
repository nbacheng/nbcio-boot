package com.nbcio.modules.estar.bs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.bs.entity.BsDataSetParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: bs_data_set_param
 * @Author: nbacheng
 * @Date:   2023-09-08
 * @Version: V1.0
 */
public interface BsDataSetParamMapper extends BaseMapper<BsDataSetParam> {

}

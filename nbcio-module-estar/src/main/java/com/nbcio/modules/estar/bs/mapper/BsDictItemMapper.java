package com.nbcio.modules.estar.bs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.bs.entity.BsDictItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: bs_dict_item
 * @Author: nbacheng
 * @Date:   2024-08-21
 * @Version: V1.0
 */
public interface BsDictItemMapper extends BaseMapper<BsDictItem> {

}

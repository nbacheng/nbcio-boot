package com.nbcio.modules.estar.bs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.bs.entity.BsFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 大屏文件
 * @Author: nbacheng
 * @Date:   2023-03-23
 * @Version: V1.0
 */
public interface BsFileMapper extends BaseMapper<BsFile> {

}

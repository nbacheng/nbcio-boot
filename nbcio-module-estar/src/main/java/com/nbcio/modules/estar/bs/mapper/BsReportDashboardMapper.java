package com.nbcio.modules.estar.bs.mapper;

import com.nbcio.modules.estar.bs.entity.BsReportDashboard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 大屏看板表
 * @Author: nbacheng
 * @Date:   2023-03-23
 * @Version: V1.0
 */
public interface BsReportDashboardMapper extends BaseMapper<BsReportDashboard> {

}

package com.nbcio.modules.estar.bs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.bs.entity.BsReportDashboardWidget;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 大屏组件表
 * @Author: nbacheng
 * @Date:   2023-03-23
 * @Version: V1.0
 */
public interface BsReportDashboardWidgetMapper extends BaseMapper<BsReportDashboardWidget> {

}

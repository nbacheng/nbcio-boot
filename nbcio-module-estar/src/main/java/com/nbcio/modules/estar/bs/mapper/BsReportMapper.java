package com.nbcio.modules.estar.bs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.bs.entity.BsReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 大屏报表
 * @Author: nbacheng
 * @Date:   2023-03-22
 * @Version: V1.0
 */
public interface BsReportMapper extends BaseMapper<BsReport> {

}

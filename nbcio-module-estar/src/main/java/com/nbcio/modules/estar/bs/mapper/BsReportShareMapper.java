package com.nbcio.modules.estar.bs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.bs.entity.BsReportShare;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 大屏分享表
 * @Author: nbacheng
 * @Date:   2023-03-23
 * @Version: V1.0
 */
public interface BsReportShareMapper extends BaseMapper<BsReportShare> {

}

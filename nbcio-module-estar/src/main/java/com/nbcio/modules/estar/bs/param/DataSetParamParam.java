/**/
package com.nbcio.modules.estar.bs.param;

import lombok.Data;

import java.io.Serializable;


/**
* @desc DataSetParam 数据集动态参数查询输入类
* @author nbacheng
* @date 2023-03-16
**/
@Data
public class DataSetParamParam extends PageParam implements Serializable{
}

/**/
package com.nbcio.modules.estar.bs.param;

import lombok.Data;
import java.io.Serializable;

import java.util.List;


/**
* @desc ReportDashboardWidget 大屏看板数据渲染查询输入类
* @author Raod
* @date 2021-04-12 15:12:43.724
**/
@Data
public class ReportDashboardWidgetParam extends PageParam implements Serializable{
}

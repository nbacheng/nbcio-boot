package com.nbcio.modules.estar.bs.service;

import com.nbcio.modules.estar.bs.entity.BsDataSetParam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: bs_data_set_param
 * @Author: nbacheng
 * @Date:   2023-09-08
 * @Version: V1.0
 */
public interface IBsDataSetParamService extends IService<BsDataSetParam> {

}

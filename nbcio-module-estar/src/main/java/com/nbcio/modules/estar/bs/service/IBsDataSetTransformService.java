package com.nbcio.modules.estar.bs.service;

import com.nbcio.modules.estar.bs.entity.BsDataSetTransform;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: bs_data_set_transform
 * @Author: nbacheng
 * @Date:   2023-09-08
 * @Version: V1.0
 */
public interface IBsDataSetTransformService extends IService<BsDataSetTransform> {

}

package com.nbcio.modules.estar.bs.service;

import com.nbcio.modules.estar.bs.entity.BsDictItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: bs_dict_item
 * @Author: nbacheng
 * @Date:   2024-08-21
 * @Version: V1.0
 */
public interface IBsDictItemService extends IService<BsDictItem> {

}

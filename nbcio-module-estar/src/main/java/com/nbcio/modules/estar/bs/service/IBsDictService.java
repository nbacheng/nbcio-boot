package com.nbcio.modules.estar.bs.service;

import com.nbcio.modules.estar.bs.bean.KeyValue;
import com.nbcio.modules.estar.bs.entity.BsDict;
import com.nbcio.modules.estar.bs.entity.BsDictItem;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: bs_dict
 * @Author: nbacheng
 * @Date:   2024-08-21
 * @Version: V1.0
 */
public interface IBsDictService extends IService<BsDict> {
	
	/**
     * 刷新全部缓存
     * @param dictCodes
     */
    void refreshCache(List<String> dictCodes);
    /**
     * 获取所有字典项
     * @return
     */
    List<BsDictItem> findItems(List<String> dictCodes);

	List<KeyValue> select(String dictCode, String language);

}

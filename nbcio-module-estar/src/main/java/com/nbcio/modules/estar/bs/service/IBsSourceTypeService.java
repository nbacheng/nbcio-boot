package com.nbcio.modules.estar.bs.service;

import com.nbcio.modules.estar.bs.entity.BsSourceType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: bs_source_type
 * @Author: nbacheng
 * @Date:   2023-03-14
 * @Version: V1.0
 */
public interface IBsSourceTypeService extends IService<BsSourceType> {

}

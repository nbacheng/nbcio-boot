package com.nbcio.modules.estar.bs.service.impl;

import com.nbcio.modules.estar.bs.entity.BsDataSetParam;
import com.nbcio.modules.estar.bs.mapper.BsDataSetParamMapper;
import com.nbcio.modules.estar.bs.service.IBsDataSetParamService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: bs_data_set_param
 * @Author: nbacheng
 * @Date:   2023-09-08
 * @Version: V1.0
 */
@Service
public class BsDataSetParamServiceImpl extends ServiceImpl<BsDataSetParamMapper, BsDataSetParam> implements IBsDataSetParamService {

}

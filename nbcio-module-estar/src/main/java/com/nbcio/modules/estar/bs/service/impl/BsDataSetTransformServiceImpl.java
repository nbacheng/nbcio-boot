package com.nbcio.modules.estar.bs.service.impl;

import com.nbcio.modules.estar.bs.entity.BsDataSetTransform;
import com.nbcio.modules.estar.bs.mapper.BsDataSetTransformMapper;
import com.nbcio.modules.estar.bs.service.IBsDataSetTransformService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: bs_data_set_transform
 * @Author: nbacheng
 * @Date:   2023-09-08
 * @Version: V1.0
 */
@Service
public class BsDataSetTransformServiceImpl extends ServiceImpl<BsDataSetTransformMapper, BsDataSetTransform> implements IBsDataSetTransformService {

}

package com.nbcio.modules.estar.bs.service.impl;

import com.nbcio.modules.estar.bs.entity.BsDictItem;
import com.nbcio.modules.estar.bs.mapper.BsDictItemMapper;
import com.nbcio.modules.estar.bs.service.IBsDictItemService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: bs_dict_item
 * @Author: nbacheng
 * @Date:   2024-08-21
 * @Version: V1.0
 */
@Service
public class BsDictItemServiceImpl extends ServiceImpl<BsDictItemMapper, BsDictItem> implements IBsDictItemService {

}

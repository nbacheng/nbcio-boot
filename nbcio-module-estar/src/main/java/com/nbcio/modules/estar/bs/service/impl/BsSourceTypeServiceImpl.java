package com.nbcio.modules.estar.bs.service.impl;

import com.nbcio.modules.estar.bs.entity.BsSourceType;
import com.nbcio.modules.estar.bs.mapper.BsSourceTypeMapper;
import com.nbcio.modules.estar.bs.service.IBsSourceTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: bs_source_type
 * @Author: nbacheng
 * @Date:   2023-03-14
 * @Version: V1.0
 */
@Service
public class BsSourceTypeServiceImpl extends ServiceImpl<BsSourceTypeMapper, BsSourceType> implements IBsSourceTypeService {

}

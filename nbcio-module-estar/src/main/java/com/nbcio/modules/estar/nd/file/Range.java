package com.nbcio.modules.estar.nd.file;

import lombok.Data;

@Data
public class Range {
    private int start;
    private int length;
}
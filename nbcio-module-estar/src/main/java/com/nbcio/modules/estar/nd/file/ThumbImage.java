package com.nbcio.modules.estar.nd.file;

import lombok.Data;

@Data
public class ThumbImage {
    private int width;
    private int height;
}

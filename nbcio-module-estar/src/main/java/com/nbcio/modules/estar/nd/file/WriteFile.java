package com.nbcio.modules.estar.nd.file;

import lombok.Data;

@Data
public class WriteFile {
    private String fileUrl;
    private long fileSize;
}

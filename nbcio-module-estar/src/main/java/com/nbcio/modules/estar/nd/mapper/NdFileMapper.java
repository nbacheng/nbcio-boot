package com.nbcio.modules.estar.nd.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.nd.entity.NdFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 网盘文件表
 * @Author: nbacheng
 * @Date:   2023-04-05
 * @Version: V1.0
 */
public interface NdFileMapper extends BaseMapper<NdFile> {

}

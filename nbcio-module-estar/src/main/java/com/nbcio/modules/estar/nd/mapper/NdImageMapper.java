package com.nbcio.modules.estar.nd.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.nd.entity.NdImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 网盘图像表
 * @Author: nbacheng
 * @Date:   2023-04-06
 * @Version: V1.0
 */
public interface NdImageMapper extends BaseMapper<NdImage> {

}

package com.nbcio.modules.estar.nd.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.nd.entity.NdUploadtask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: nd_uploadtask
 * @Author: nbacheng
 * @Date:   2023-04-08
 * @Version: V1.0
 */
public interface NdUploadtaskMapper extends BaseMapper<NdUploadtask> {

}

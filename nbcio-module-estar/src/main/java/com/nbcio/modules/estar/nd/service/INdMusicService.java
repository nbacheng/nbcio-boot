package com.nbcio.modules.estar.nd.service;

import com.nbcio.modules.estar.nd.entity.NdMusic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 网盘音乐表
 * @Author: nbacheng
 * @Date:   2023-04-06
 * @Version: V1.0
 */
public interface INdMusicService extends IService<NdMusic> {

}

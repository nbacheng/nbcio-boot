package com.nbcio.modules.estar.nd.service.impl;

import com.nbcio.modules.estar.nd.entity.NdMusic;
import com.nbcio.modules.estar.nd.mapper.NdMusicMapper;
import com.nbcio.modules.estar.nd.service.INdMusicService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 网盘音乐表
 * @Author: nbacheng
 * @Date:   2023-04-06
 * @Version: V1.0
 */
@Service
public class NdMusicServiceImpl extends ServiceImpl<NdMusicMapper, NdMusic> implements INdMusicService {

}

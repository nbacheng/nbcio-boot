package com.nbcio.modules.estar.nd.service.impl;

import com.nbcio.modules.estar.nd.entity.NdPicturefile;
import com.nbcio.modules.estar.nd.mapper.NdPicturefileMapper;
import com.nbcio.modules.estar.nd.service.INdPicturefileService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: nd_picturefile
 * @Author: nbacheng
 * @Date:   2023-04-08
 * @Version: V1.0
 */
@Service
public class NdPicturefileServiceImpl extends ServiceImpl<NdPicturefileMapper, NdPicturefile> implements INdPicturefileService {

}

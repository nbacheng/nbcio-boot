package com.nbcio.modules.estar.nd.service.impl;

import com.nbcio.modules.estar.nd.entity.NdSysparam;
import com.nbcio.modules.estar.nd.mapper.NdSysparamMapper;
import com.nbcio.modules.estar.nd.service.INdSysparamService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: nd_sysparam
 * @Author: nbacheng
 * @Date:   2023-04-08
 * @Version: V1.0
 */
@Service
public class NdSysparamServiceImpl extends ServiceImpl<NdSysparamMapper, NdSysparam> implements INdSysparamService {

}

package com.nbcio.modules.estar.nd.service.impl;

import com.nbcio.modules.estar.nd.entity.NdUploadtask;
import com.nbcio.modules.estar.nd.mapper.NdUploadtaskMapper;
import com.nbcio.modules.estar.nd.service.INdUploadtaskService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: nd_uploadtask
 * @Author: nbacheng
 * @Date:   2023-04-08
 * @Version: V1.0
 */
@Service
public class NdUploadtaskServiceImpl extends ServiceImpl<NdUploadtaskMapper, NdUploadtask> implements INdUploadtaskService {

}

package com.nbcio.modules.estar.nd.service.impl;

import com.nbcio.modules.estar.nd.entity.NdUploadtaskdetail;
import com.nbcio.modules.estar.nd.mapper.NdUploadtaskdetailMapper;
import com.nbcio.modules.estar.nd.service.INdUploadtaskdetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: nd_uploadtaskdetail
 * @Author: nbacheng
 * @Date:   2023-04-08
 * @Version: V1.0
 */
@Service
public class NdUploadtaskdetailServiceImpl extends ServiceImpl<NdUploadtaskdetailMapper, NdUploadtaskdetail> implements INdUploadtaskdetailService {

}

package com.nbcio.modules.estar.oa.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.oa.entity.OaSchedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: OA日程表
 * @Author: nbacheng
 * @Date:   2023-05-04
 * @Version: V1.0
 */
public interface OaScheduleMapper extends BaseMapper<OaSchedule> {

}

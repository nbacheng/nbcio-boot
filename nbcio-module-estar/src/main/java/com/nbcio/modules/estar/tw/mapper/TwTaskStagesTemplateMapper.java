package com.nbcio.modules.estar.tw.mapper;

import com.nbcio.modules.estar.tw.entity.TwTaskStagesTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 任务列表模板表
 * @Author: nbacheng
 * @Date:   2023-05-29
 * @Version: V1.0
 */
public interface TwTaskStagesTemplateMapper extends BaseMapper<TwTaskStagesTemplate> {

}

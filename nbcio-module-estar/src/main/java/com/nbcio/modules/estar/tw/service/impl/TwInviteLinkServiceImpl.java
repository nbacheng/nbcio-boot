package com.nbcio.modules.estar.tw.service.impl;

import com.nbcio.modules.estar.tw.entity.TwInviteLink;
import com.nbcio.modules.estar.tw.mapper.TwInviteLinkMapper;
import com.nbcio.modules.estar.tw.service.ITwInviteLinkService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目邀请链接表
 * @Author: nbacheng
 * @Date:   2023-07-03
 * @Version: V1.0
 */
@Service
public class TwInviteLinkServiceImpl extends ServiceImpl<TwInviteLinkMapper, TwInviteLink> implements ITwInviteLinkService {

}
